# vim: ft=perl

$out_dir = 'output';
if ($^O eq 'linux') {
    $pdf_previewer = 'zathura';
} elsif ($^O eq 'MSWin32') {
    $pdf_previewer = 'mupdf'
}
$bibtex_use = 2;
$pdf_mode = 1; $postscript_mode = $dvi_mode = 0;
@default_files = ('build-cache/resume.tex', 'build-cache/cover.tex');
$clean_ext = 'ent run.xml';
$cleanup_includes_cusdep_generated = 1;
$cleanup_includes_generated = 1;
$pdflatex = 'internal mylatex %S %O';

sub mylatex {
    my ($base_name, $path, $ext) = fileparse(shift, ('.tex'));
    my @args = @_;

    push(@args,
        '-interaction=nonstopmode'#,
        #'-no-pdf'
        #'-output-driver="xdvipdfmx -z0"'
    );

    # Generate .git/gitHeadInfo.gin
    # http://ctan.math.washington.edu/tex-archive/macros/latex/contrib/gitinfo2/gitinfo2.pdf
    #system('./gitinfo.sh');

    return system('xelatex', @args, $path . $base_name . $ext);
}

# TODO: Replace shell script
sub gitinfo {
    my $first_tag = `git describe --tags --always --dirty='-*'`;
    my $rel_tag = `git describe --tags --long --always --dirty='-*' --match '[0-9]*.*'`;
}

# {{{ Make Glossary
add_cus_dep('glo', 'gls', 0, 'makegloss');
add_cus_dep('acn', 'acr', 0, 'makegloss');
sub makegloss {
    my ($base_name, $path) = fileparse($_[0]);
    if ($silent) {
        system("makeglossaries -q -d $path $base_name");
    } else {
        system("makeglossaries -d $path $base_name");
    };
}
push @generated_exts, 'ist', 'glo', 'gls', 'glg', 'acn', 'acr', 'alg';
# }}}

# {{{ Make Index
add_cus_dep('idx', 'ind', 0, 'makeindex');
sub makeindex {
    system("makeindex \"$_[0].idx\"");
}
# }}}

# {{{ Gnuplot figures
add_cus_dep('gnuplot', 'tex', 0, 'makegnuplot');
sub makegnuplot {
    return system("gnuplot \"$_[0].gnuplot\"");
}
# }}}

# {{{ Inkscape to convert SVG to PDF/EPS or PDF/EPS+TEX
add_cus_dep('svg', 'pdf_tex', 0, 'svg2pdf_tex');
sub svg2pdf_tex {
    return system("inkscape --without-gui --export-area-drawing --export-latex --export-pdf=\"$_[0].pdf\" \"$_[0].svg\"");
}

add_cus_dep('svg', 'pdf', 0, 'svg2pdf');
sub svg2pdf {
    return system("inkscape --without-gui --export-area-drawing --export-pdf=\"$_[0].pdf\" \"$_[0].svg\"");
}

add_cus_dep('svg', 'eps_tex', 0, 'svg2eps_tex');
sub svg2eps_tex {
    return system("inkscape --without-gui --export-area-drawing --export-latex --export-eps=\"$_[0].eps\" \"$_[0].svg\"");
}

add_cus_dep('svg', 'eps', 0, 'svg2eps');
sub svg2eps {
    return system("inkscape --without-gui --export-area-drawing --export-eps=\"$_[0].eps\" \"$_[0].svg\"");
}
# }}}

# {{{ Ghostscript to convert Adobe Illustrator to PDF/EPS
add_cus_dep('ai', 'pdf', 0, 'ai2pdf');
sub ai2pdf {
    return system("gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=\"$_[0].pdf\" \"$_[0].ai\"");
}

add_cus_dep('ai', 'eps', 0, 'ai2eps');
sub ai2eps {
    return system("gs -dNOPAUSE -dBATCH -sDEVICE=eps2write -sOutputFile=\"$_[0].eps\" \"$_[0].ai\"");
}
# }}}
